const fs = require("fs");
const chalk = require('chalk')

const getNotes = ()=>{
    return `Your Notes...`
}
//inserting new notes
const addNotes = (title,body)=>{
    const notes = loadNotes()
    const duplicate =notes.filter(note=>{
            return note.title === title
        })
    

    if(duplicate.length === 0){
    
        notes.push({
            title:title,
            body:body
        })
     
        saveNotes(notes)
        console.log(chalk.red.bgGreen.bold("note Added"));
    
    }else{
        console.log("Note Already Exist");
    }
}
//loading notes.
const loadNotes = ()=>{
    try{
        const dataBuffer = fs.readFileSync('./notes/notes.json')
        const data =  dataBuffer.toString();
        return JSON.parse(data)
    }catch(e){
        return []
    }
}
//save notes in file
const saveNotes = (notes)=>{
    const data = JSON.stringify(notes)
    fs.writeFileSync(`./notes/notes.json`,data)    
}
//deleting notes
const removeNotes= (title)=>{
    const notes = loadNotes()
    delete notes.title === title;
    saveNotes(notes)
    console.log(chalk.white.bgRed.bold("Notes with title "+title+" Deleted."));
}
//listing notes
const listNotes =()=>{
    const notes = loadNotes()
    notes.filter(note=>console.log(chalk.red.bgGreen.bold(note.title)))
}
//read notes
const readNotes = (title)=>{
    const note = loadNotes();
    note.forEach(note => {
        if(note.title === title){
            console.log(chalk.green.bgRed.bold(note.body))
        }
    });
}
module.exports = {
    addNotes:addNotes,
    getNotes:getNotes,
    removeNotes:removeNotes,
    listNotes:listNotes,
    readNotes:readNotes
};