const yargs = require('yargs')
const notes = require('./notes')
yargs.version('1.1.0')
//defining yargs command for add,remove,list,read
yargs.command({
    command:"add",
    describe:'Add New Note',
    builder:{
        title:{
            describe:"Notes Title"
        },
        body:{
            describe:"Notes Body"
        }
    },  
    handler:(argv)=>{
        notes.addNotes(argv.title,argv.body)        
    }
});

yargs.command({
    command:"remove",
    describe:'Remove Note',
    builder:{
        title:{
            describe:"Remove Notes"
        }
    },
    handler:(argv)=>{
        notes.removeNotes(argv.title)
    }
});


yargs.command({
    command:"list",
    describe:'list Note',
    handler:()=>{
        notes.listNotes()
    }
});


yargs.command({
    command:"read",
    describe:'Read Note',
    builder:{
        title:{
            describe:"Read Notes"
        }
    },
    handler:(argv)=>{
        notes.readNotes(argv.title)
    }
});

yargs.parse()